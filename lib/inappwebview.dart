import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class InAppWebview extends StatefulWidget {
  @override
  _InAppWebviewState createState() => new _InAppWebviewState();
}

class _InAppWebviewState extends State<InAppWebview> {
  InAppWebViewController webView;
  String url = "";
  double progress = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Citi Designer'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Container(
          child: Column(children: <Widget>[
        Expanded(
          child: Container(
            margin: const EdgeInsets.all(2.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.blueAccent)),
            child: InAppWebView(
              initialUrl: "https://citidesigner.com/login",
              initialHeaders: {},
              initialOptions: InAppWebViewGroupOptions(
                  crossPlatform: InAppWebViewOptions(
                debuggingEnabled: true,
              )),
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onLoadStart: (InAppWebViewController controller, String url) {
                setState(() {
                  this.url = url;
                });
              },
              onLoadStop:
                  (InAppWebViewController controller, String url) async {
                setState(() {
                  this.url = url;
                });
              },
              onProgressChanged:
                  (InAppWebViewController controller, int progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
            ),
          ),
        ),
        ButtonBar(
          alignment: MainAxisAlignment.center,
          children: <Widget>[
            // RaisedButton(
            //   child: Icon(Icons.arrow_back),
            //   onPressed: () {
            //     if (webView != null) {
            //       if (this.url == "https://citidesigner.com/login") {
            //         print("cant go back");
            //       } else {
            //         print("can go back");
            //         webView.goBack();
            //       }
            //     }
            //   },
            // ),
            // RaisedButton(
            //   child: Icon(Icons.arrow_forward),
            //   onPressed: () {
            //     if (webView != null) {
            //       webView.goForward();
            //     }
            //   },
            // ),
            // RaisedButton(
            //   child: Icon(Icons.refresh),
            //   onPressed: () {
            //     if (webView != null) {
            //       webView.reload();
            //     }
            //   },
            // ),
          ],
        ),
      ])),
    );
  }
}
